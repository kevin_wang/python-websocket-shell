#!/usr/bin/expect
set work "/work/ModemWarface/android/zsxd-client"
set keypath "/work/ModemWarface/shell/.mykey/"
set zdate [exec date +%Y%m%d-%H%M%S]

#写日志的函数
proc log {msg} {
#写日志的同时将消息打印在屏幕上
        puts "$msg\n"
        send_log "$msg\n"
}

log_file "$work/../shell/log/export-apk.log"
set timeout 7200

cd $work


spawn bash -c "svn revert -R $work/Assets/"
expect eof
spawn bash -c "cp $work/../../config/android/Config.cs $work/Assets/Scripts/Config/Config.cs"
expect eof

spawn bash -c "cp $work/../../config/android/ExportAssetBundles.cs $work/Assets/Crypto/ExportAssetBundles.cs"
expect eof

spawn svn up
expect eof

spawn cp $work/../../config/bundle/android.txt $work/Assets/BundleManager/Resources/Urls.txt
expect eof
spawn bash -c "find $work/Assets/Plugins/NGUI/ -name 'Editor'  -type d -print | xargs /bin/rm -r -f"
expect eof
spawn bash -c "find $work/Assets/PSD2NGUI/ -name 'Editor'  -type d -print | xargs /bin/rm -r -f"
expect eof


spawn "/Applications/Unity/Unity.app/Contents/MacOS/Unity" -quit -batchmode -projectPath "$work" -executeMethod ExportAssetBundles.BuildIOS -logFile "$work/../shell/log/export-app.log"
expect eof

cd $work/build/

spawn scp -i $keypath/id_dsa_1024_114 $work/build/xdzz.apk root@192.168.1.2:/home/zhulangren/xdzz/android/xdzz-$zdate.apk
expect {Enter passphrase for key}
send "********\n"

expect eof
log "upload ipa file"

puts "end!!!!"
