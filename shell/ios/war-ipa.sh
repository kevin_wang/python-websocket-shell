#!/usr/bin/expect
set work "/work/ModemWarface/zsxd-client/"
set zdate [exec date +%Y%m%d-%H%M%S]

#写日志的函数
proc log {msg} {
#写日志的同时将消息打印在屏幕上
        puts "$msg\n"
        send_log "$msg\n"
}

log_file "$work/../shell/log/export-ipa.log"
set timeout 3600

cd $work


spawn bash -c "svn revert -R $work/Assets/"
expect eof
spawn bash -c "cp $work/../config/ios/Config.cs $work/Assets/Scripts/Config/Config.cs"
expect eof

spawn bash -c "cp $work/../config/ios/ExportAssetBundles.cs $work/Assets/Crypto/ExportAssetBundles.cs"
expect eof

spawn svn up
expect eof

spawn cp $work/../config/bundle/ios.txt $work/Assets/BundleManager/Resources/Urls.txt
expect eof

spawn bash -c "find $work/Assets/Plugins/NGUI/ -name 'Editor'  -type d -print | xargs /bin/rm -r -f"
expect eof
spawn bash -c "find $work/Assets/PSD2NGUI/ -name 'Editor'  -type d -print | xargs /bin/rm -r -f"
expect eof

spawn "/Applications/Unity/Unity.app/Contents/MacOS/Unity" -quit -batchmode -projectPath "$work" -executeMethod ExportAssetBundles.BuildIOS -logFile "$work/../shell/log/export-app.log"
expect eof

spawn bash -c "cp -rf $work/../config/none/* $work/build/ios/"
expect eof


cd $work/build/ios/

spawn security unlock-keychain
expect ":"
send "********\n"
expect eof
log "unlock key"


spawn xcodebuild -target Unity-iPhone -configuration Debug -sdk iphoneos clean build
expect eof
log "xcodebuild succeeded"

spawn xcrun -sdk iphoneos  PackageApplication -v $work/build/ios/build/xdzz.app -o $work/build/xdzz.ipa
expect eof
log "xcrun scceeded"

spawn scp -i $work/../shell/.mykey/id_dsa_1024_114 $work/build/xdzz.ipa root@192.168.1.2:/home/zhulangren/xdzz/ios/xdzz-$zdate.ipa
expect {Enter passphrase for key}
send "********\n"
expect eof
log "upload ipa file"

puts "end!!!!"
