#!/usr/bin/expect
set work "/work/ModemWarface/dataassets/"
log_file "$work/../shell/log/export-locale-data.log"
set timeout 3600

spawn "/work/ModemWarface/shell/svn-up.sh"
expect eof
spawn "/Applications/Unity/Unity.app/Contents/MacOS/Unity" -quit -batchmode -projectPath "$work" -executeMethod ExportAssetBundles.ExportResourceTrack -logFile "$work/../shell/log/export-locale-data1.log"
expect eof
cd $work/Assets/temp/

spawn bash -c "python /work/ModemWarface/shell/python-tool/version.py"
expect eof

spawn bash -c "find $work/Assets/temp/ -name '*.meta'  -type f -print | xargs /bin/rm -f"
expect eof
spawn bash -c "scp -r -i $work/../shell/.mykey/id_dsa_1024_114  $work/Assets/temp/* root@192.168.1.2:/var/www/tankwar/res/"
expect {Enter passphrase for key}
send "********\n"
expect eof
puts "end!!!!"
