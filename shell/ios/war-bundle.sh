#!/usr/bin/expect
set platform [lindex $argv 0]

set work "/work/ModemWarface/zsxdres/"
set conwork "/work/ModemWarface/"
if {$platform=="win"} {
    set work "/work/ModemWarface/zsxdres-win/"
    set conwork "/work/ModemWarface/"
}
if {$platform=="android"} {
    set work "/work/ModemWarface/android/zsxdres/"
    set conwork "/work/ModemWarface/android/"
}


log_file "$work/../shell/log/export-bundle.log"
set timeout 3600


spawn bash -c "svn revert -R $work/Assets/BundleManager/"
expect eof
  
spawn "/work/ModemWarface/shell/svn-up.sh"
expect eof


spawn "/Applications/Unity/Unity.app/Contents/MacOS/Unity" -quit -batchmode -projectPath "$conwork/dataassets/" -executeMethod ExportAssetBundles.ExportResourceTrack -logFile "$conwork/shell/log/export-data1.log"
expect eof
spawn cp $conwork/dataassets/Assets/temp/dataconfig/dataconfig.bytes $work/Assets/XingYu/data/
expect eof

if {$platform=="win"} {
    spawn bash -c "svn ci $work/Assets/XingYu/data/dataconfig.bytes -m配置数据重新打包"
    expect eof
}
spawn cp /work/ModemWarface/config/bundle/$platform.txt $work/Assets/BundleManager/Resources/Urls.txt
expect eof


spawn "/Applications/Unity/Unity.app/Contents/MacOS/Unity" -quit -batchmode -projectPath "$work" -executeMethod BuildHelper.BuildAll -logFile "$work/../shell/log/export-bundle1.log"
expect eof

spawn "/Applications/Unity/Unity.app/Contents/MacOS/Unity" -quit -batchmode -projectPath "$work" -executeMethod BuildHelper.ExportBMDatasToOutput -logFile "$work/../shell/log/export-bundle2.log"
expect eof
if {$platform=="win"} {
    spawn bash -c "svn ci $work/Assets/BundleManager/BuildStates.txt -m配置数据重新打包"
    expect eof
}
spawn bash -c "scp -r -i /work/ModemWarface/shell/.mykey/id_dsa_1024_114  $work/AssetBundle/* root@192.168.1.2:/var/www/tankwar/"
expect {Enter passphrase for key}
send "********\n"
expect eof


spawn ssh -i /work/ModemWarface/shell/.mykey/id_dsa_1024_114 root@192.168.1.2
expect {Enter passphrase for key}
send "********\n"
expect "#"
send "/work/tool/scp_178.sh\n"
expect "bison-end"

send "exit\n"
puts "end\n"
exit
